var main_8py =
[
    [ "closedMotor", "main_8py.html#ac98ba2523ea347688d5871b2f3ae94a1", null ],
    [ "correct", "main_8py.html#a5e032d3667305670bee1b27aff73dcbc", null ],
    [ "curPos", "main_8py.html#aba09e9c7d11a5f3b5fa64269c08a8b0f", null ],
    [ "delta", "main_8py.html#a01cf4e8a64081698689afb33f0fc217d", null ],
    [ "enA", "main_8py.html#a5dc70cc8ea3060c83f9cf9951758c2f3", null ],
    [ "enable", "main_8py.html#ae15862523d5df36866a2de21a64e936b", null ],
    [ "enB", "main_8py.html#a60627ea9c92ec47eb4eb51e3dced168d", null ],
    [ "encoder", "main_8py.html#ace31ebc62141759cb9abfc385476bef0", null ],
    [ "entimer", "main_8py.html#ad60c71e0600710929f0c077ea2c73cf5", null ],
    [ "kp", "main_8py.html#af902ad7505407a7439de68bf877cd303", null ],
    [ "lastUpdate", "main_8py.html#afeff24fa59917d9712b2f519cd336dcf", null ],
    [ "logPositions", "main_8py.html#af33f9fd0f3ff01b9418e73489a5873f6", null ],
    [ "logTimes", "main_8py.html#aa8659e1edf581ab1ca09984bf2f6cbf7", null ],
    [ "m1", "main_8py.html#a5b9e35e46d9ca83008791495f109c6a6", null ],
    [ "m2", "main_8py.html#a3116322dad9ca0083c4d0551e2391587", null ],
    [ "motor", "main_8py.html#aa5e57dc8139b8c85f29df12d0ead4edd", null ],
    [ "mtimer", "main_8py.html#aa802ed3dd59c4dc9f94659d46c4dc9c9", null ],
    [ "positions", "main_8py.html#a60c3a6b79dbb65715f4d24dca975d12f", null ],
    [ "startTime", "main_8py.html#a2a3c1bda9de5f02059bdc278a67a4116", null ],
    [ "time", "main_8py.html#a7047ceda29820f67f4b6c0d012764681", null ],
    [ "WIGGLE_ROOM", "main_8py.html#a9604e2a0db9cfe4e32dc170686c7e79e", null ]
];
''' @file main.py
The file containing the main function of ME405 - Lab 3.

@author James McRoberts
@copyright 2020
@date May 15th, 2020
@package main'''

import pyb
import utime

from encoder import *
from motor import *
from closedmotor import *

# Program config
WIGGLE_ROOM = 3

# Set up motor
enable = pyb.Pin(pyb.Pin.cpu.A4, pyb.Pin.OUT_PP)
mtimer = pyb.Timer(5, freq=20000)
m1 = pyb.Pin(pyb.Pin.cpu.A0)
m2 = pyb.Pin(pyb.Pin.cpu.A1)

motor = MotorDriver(enable, m1, m2, mtimer)
motor.enable()

# Set up encoder
enA = pyb.Pin.cpu.C6
enB = pyb.Pin.cpu.C7
entimer = 8

encoder = MotorEncoder(enA, enB, entimer, None)

# Set up closed loop motor controller
closedMotor = ClosedMotor(motor, encoder)


# The main loop
kp = input('Enter kp val: ')
kp = float(kp)
positions = [-500, 500]
logTimes = []
logPositions = []
startTime = utime.ticks_ms()
lastUpdate = startTime

# Iterate through target positions
for pos in positions:
    closedMotor.setPos(pos)
    correct = 0
    
    # Loop
    curPos = closedMotor.getPos()
    
    while (curPos <= pos + WIGGLE_ROOM and curPos >= pos - WIGGLE_ROOM) or correct < 10:
        closedMotor.update(kp)
        time = utime.ticks_ms()
        
        if curPos <= pos + WIGGLE_ROOM and curPos >= pos - WIGGLE_ROOM:
            correct += 1
            logTimes.append(utime.ticks_ms() - startTime)
            logPositions.append(curPos)
            lastUpdate = time
        
        delta = time - lastUpdate
        if delta > 10:
            lastUpdate = time
            logTimes.append(utime.ticks_ms() - startTime)
            logPositions.append(curPos)
        
        curPos = closedMotor.getPos()
    
    # Delay
    utime.sleep(1)
    
# Print out results
for i in range(len(logTimes)):
    print(str(logTimes[i]) + ', ' + str(logPositions[i]))

''' @file closedmotor.py
Defines a class for working with closed loop motor control systems.

@author James McRoberts
@copyright 2020
@date May 15th, 2020
@package closedmotor'''

import encoder
import motor

class ClosedMotor:
    ''' This class implements methods to properly assemble a closed loop motor control
    system. '''
    
    # The basic components of the system
    motor = None
    encoder = None
    setpoint = 0
    __position = 0
    
    def __init__(self, motor, encoder):
        ''' This class links a motor object and an encoder object t create a closed
        loop control system.
        @param motor The motor to use in the system.
        @param encoder The encoder used for feedback from the motor, this will be zeroed. '''
        print('Creating a closed loop motor controller.')
        
        # Assign to self
        self.motor = motor
        self.encoder = encoder
        self.encoder.set_position(0)
        
    def getPos(self):
        ''' This function is a wrapper to get the encoder value, updating the encoder. '''
        self.encoder.update()
        self.__position = self.__position + self.encoder.get_delta()
        return self.__position
    
    def setPos(self, setpoint):
        ''' This function sets the system, logically, to the position provided.
        @param setpoint The position to get to. '''
        self.setpoint = setpoint
        
    def update(self, kp):
        ''' Uses the parameters provided in prior method calls to run one control cycle
        of the closed loop system on the objects provided during construction.
        @param kp The kp parameter of the basic PID loop. '''
        # Extract parameters
        curPos = self.getPos()
        errSignal = self.setpoint - curPos
        actSignal = errSignal * kp
        if actSignal > 100: actSignal = 100
        elif actSignal < -100: actSignal = -100
        
        # Apply data
        self.motor.setDuty(-actSignal)

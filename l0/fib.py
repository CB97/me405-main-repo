''' @file fib.py
Functions used to work with the Fibonacci Sequence, specifically the retrieval
of an element at an index. '''

__fibNums = [1, 1]

def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to a specified index.
    @param idx An integer specifying the index of the desired Fibonacci number. '''
    print('Calculating Fibonacci number at index n = {:}.'.format(idx))

    # Expand memory till result computed
    current = len(__fibNums)
    while (current <= idx):
        # Compute and iter, not for because of weird expanding nature
        __fibNums.append(__fibNums[current - 1] + __fibNums[current - 2])
        current = current + 1

    return __fibNums[idx]

if __name__ == '__main__':
    # Give a way to quit
    print("Enter \"q\" to exit.")

    # User interface -> rubric
    while (1):
        # User interface -> rubric
        index = input("Enter the index of the Fibonacci to calculate: ")

        # A way to quit without errors
        if (index == "q"):
            break

        # Error handling -> rubric
        try:
            index = int(index)
        except ValueError:
            print("Error: ({:}) is not a valid index, must be a number".format(index))
            continue
        if (index < 0):
            print("Error: The index ({:}) must be >= 0".format(index))
            continue

        # Print result
        result = fib(index)
        print("Fibonacci number at index ({:}): {:}".format(index, result))
        
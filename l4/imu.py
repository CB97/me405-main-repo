''' @file imu.py
Defines a class for working with the BNO055 IMU.

@author James Hamilton McRoberts IV
@copyright 2020
@date May 16th, 2020
@package imu '''

from pyb import I2C

class imuDriver:
    ''' This class defines a driver to work with the BNO055 IMU. '''
    # Constants
    ADDRESS = 40
    UNIT_SEL = 0x3B
    OPR_MODE = 0x3D
    
    # Config modes
    CONFIGMODE = 0
    ACCONLY = 1
    MAGONLY = 2
    GYROONLY = 3
    ACCMAG = 4
    ACCGYRO = 5
    MAGGYRO =  6
    AMG = 7
    
    IMU = 8
    COMPASS = 9
    M4G = 10
    NDOF_FMC_OFF = 11
    NDOF = 12
    
    # Internal variables
    i2cBus = None
    mode = CONFIGMODE
    
    linear = [0.0, 0.0, 0.0]
    rotation = [0.0, 0.0, 0.0]
    euler = [0.0, 0.0, 0.0]
    
    sysCalib = 0
    gyrCalib = 0
    accCalib = 0
    magCalib = 0
    
    def __init__(self, i2c):
        ''' Bind the I2C channel to the IMU. Sets the IMU to operate in exclusively
        metric. Imperial is metric anyways. Also degrees are better, no question.
        @param i2c The I2C object, holding the channel, to use for the IMU.'''
        print('Creating IMU driver.')
        
        if not self.ADDRESS in i2c.scan():
            print('Could not create IMU driver, no IMU found.')
            return
        
        self.i2cBus = i2c
        
        # Set units
        units = 0 << 0 # m/s2
        units = units | (0 << 1) # dps
        units = units | (0 << 2) # degrees
        units = units | (0 << 4) # C
        i2c.mem_write(units, self.ADDRESS, self.UNIT_SEL)

    def setMode(self, mode):
        ''' This function sets the operating mode of the IMU with a passed
        constant from this class.
        @param mode One of the mode constants from this class. '''
        oldByte = self.i2cBus.mem_read(1, self.ADDRESS, self.OPR_MODE)[0]
        oldByte = (oldByte & 0xF0) | (mode & 0x0F)
        self.i2cBus.mem_write(oldByte, self.ADDRESS, self.OPR_MODE)
        self.mode = mode

    def getMode(self):
        ''' This function gets the operating mode of the IMU. Note, this only works if the
        IMU has only been interacted with by this driver.
        @returns The mode constant. '''
        return self.mode
    
    def enable(self):
        ''' Enables the IMU by setting the operating mode to NDOF. '''
        self.setMode(self.NDOF)
    
    def disable(self):
        ''' Disables the IMU by setting the operating mode to CONFIGMODE. '''
        self.setMode(self.CONFIGMODE)
        
    def update(self):
        ''' Updates the internal states of the IMU class. '''
        # Handle calibration
        calibRaw = self.i2cBus.mem_read(1, self.ADDRESS, 0x35)[0]
        magCalib = calibRaw & 0b11
        accCalib = (calibRaw >> 2) & 0b11
        gyrCalib = (calibRaw >> 4) & 0b11
        sysCalib = (calibRaw >> 6) & 0b11
        
        # Handle movement
        linMem = [0x08, 0x0A, 0x0C]
        rotMem = [0x14, 0x16, 0x18]
        eulMem = [0x1A, 0x1C, 0x1E]
        
        for i in range(3):
            self.linear[i] = int.from_bytes(self.i2cBus.mem_read(2, self.ADDRESS, linMem[i]), 'little')
            if self.linear[i] > 32767: self.linear[i] = -(0xFFFF - self.linear[i] + 1)
            self.linear[i] = float(self.linear[i]) / 100
            
            self.rotation[i] = int.from_bytes(self.i2cBus.mem_read(2, self.ADDRESS, rotMem[i]), 'little')
            if self.rotation[i] > 32767: self.rotation[i] = -(0xFFFF - self.rotation[i] + 1)
            self.rotation[i] = float(self.rotation[i]) / 100
            
            self.euler[i] = int.from_bytes(self.i2cBus.mem_read(2, self.ADDRESS, eulMem[i]), 'little')
            if self.euler[i] > 32767: self.euler[i] = -(0xFFFF - self.euler[i] + 1)
            self.euler[i] = float(self.euler[i]) / 100
        
    def getAccel(self):
        ''' Gets the acceleration variables measured by the IMU.
        @returns The acceleration rotationally (a rank 3 tuple organized x, y, z),
            the acceleration linearly (a rank 3 tuple organized x, y, z), and the Euler
            angles (a rank 3 tuple organized x, y, z). '''
        return [self.rotation, self.linear, self.euler]
    
    def getCalib(self):
        ''' Gets the calibration states for the IMU.
        @returns An array of the sys, gyro, accelerometer, and magnometer calibration
        statuses respectively. '''
        return [self.sysCalib, self.gyrCalib, self.accCalib, self.magCalib]

if __name__ == '__main__' or __name__ == 'builtins':
    from utime import sleep
    
    i2c = I2C(1, I2C.MASTER)
    imu = imuDriver(i2c)
    imu.enable()
    
    while True:
        imu.update()
        accel = imu.getAccel()
        calib = imu.getCalib()
        
        rot = accel[0]
        lin = accel[1]
        eul = accel[2]
        
        print('')
        print('Rot: [' + str(rot[0]) + ', ' + str(rot[1]) + ', ' + str(rot[2]) + ']')
        print('Lin: [' + str(lin[0]) + ', ' + str(lin[1]) + ', ' + str(lin[2]) + ']')
        print('Eul: [' + str(eul[0]) + ', ' + str(eul[1]) + ', ' + str(eul[2]) + ']')
        
        sleep(1)
        
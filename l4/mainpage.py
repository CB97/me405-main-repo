''' @file mainpage.py
@author James McRoberts
@mainpage
    
@section intro Introduction
This software is designed to ease the use of the BNO055 9-DOF IMU chipset. It
should ease the conversion between very C like structures, and pythons... python...
structures.

@section purpose Purpose
The purpose of this lab is to make a framework for ease of use of the IMU for use
in later projects. On top of that, I2C is being demonstrated from a micropython
standpoint.

@section usage Usage
To create the IMU driver, the imuDriver() constructor is called. The only thing
that should be needed to be provided to the constructor is the contructed I2C
bus. After that, to get any useful data inside of the driver, update() must be
called. From there, the various other methods in the class are pretty self
explanatory in their use cases.

@section buglimits Bugs and Limitations
After continued testing, no bugs have been found.

@section video Video
To see a video of the IMU in action, the video can be found at
https://youtu.be/HCW1i5PGByc

@section location Location
To get the code for the project, the repository can be found at
https://bitbucket.org/CB97/me405-main-repo/src/master/l4/'''

''' @file raspiHead.py
Defines a class for listening to the raspberry pi.

@author James Hamilton McRoberts IV
@copyright 2020
@date June 10th, 2020
@package final '''

import pyb
from pyb import I2C
import struct
from imu import IMUDriver
from motor import MotorDriver
from encoder import MotorEncoder
from closedmotor import ClosedMotor

class RaspiHead:
    ''' An object for receiving direction commands from the head of the robot. '''
    bus = None
    imu = None
    
    def __init__(self, uart, imu):
        ''' Bind the serial channel to the raspi head.
        @param uart The UART channel to use to communicate to the MCU.
        @param imu The initialized IMU object to pull data from on the robot head.'''
        print('Connecting to raspi head.')
        
        self.bus = uart
        self.imu = imu
    
        
    def update(self):
        ''' Reads from the raspi to find the current states of the head. '''
        results = []
        
        count = self.bus.read(struct.calcsize('i'))
        if count == None: return None
        val = struct.unpack('i', count)
        
        for idx in range(val):
            face = self.bus.read(struct.calcsize('iii'))
            x, y, a = struct.unpack('iii', face)
            results.append((x, y), a)
            
        imu.update()
        rot = self.imu.getAccel()[1]
        return results, rot
        

if not(__name__ == '__main__' or __name__ == 'builtins'): exit()
def __reactRot(motorP, motorT, rot):
    print('rot')
    pPos = motorP.getPos()
    tPos = motorT.getPos()
    pVal = rot[2] + pPos
    tVal = rot[1] + tPos
    
    motorP.setPos(pVal)
    motorT.setPos(tVal)
    motorP.update(10, 0, 0)
    motorT.update(10, 0, 0)

def __reactCam(motorP, motorT, pos):
    print('cam')
    pPos = motorP.getPos()
    tPos = motorT.getPos()
    pVal = pos[0] + pPos
    tVal = pos[1] + tPos
    
    motorP.setPos(pVal)
    motorT.setPos(tVal)
    motorP.update(10, 0, 0)
    motorT.update(10, 0, 0)
    

''' Main function area '''
# Running data
i2c = I2C(1, I2C.MASTER)
imu = IMUDriver(i2c)
imu.enable()
bus = pyb.UART(3, 9600)
head = RaspiHead(bus, imu)

# Init movement
pchA = pyb.Pin.cpu.B4
pchB = pyb.Pin.cpu.B5
pencA = pyb.Pin.cpu.A11
pencB = pyb.Pin.cpu.A12
penable = pyb.Pin.cpu.A10
ptimer = pyb.Timer(3, freq=20000)
pencTimer = 1
tchA = pyb.Pin.cpu.A0
tchB = pyb.Pin.cpu.A1
tencA = pyb.Pin.cpu.C6
tencB = pyb.Pin.cpu.C8
tenable = pyb.Pin.cpu.C1
ttimer = pyb.Timer(5, freq=20000)
tencTimer = 8
pmotP = MotorDriver(penable, pchA, pchB, ptimer)
tmotP = MotorDriver(tenable, tchA, tchB, ttimer)
pencP = MotorEncoder(pencA, pencB, pencTimer, None)
tencP = MotorEncoder(tencA, tencB, tencTimer, None)
pmot = ClosedMotor(pmotP, pencP)
tmot = ClosedMotor(tmotP, tencP)

while True:
    # Look for the biggest face
    temp = head.update()
    if temp == None: continue
    print(1)
    positions, rotation = temp
    largestPos = None
    largestA = 0
    for (x, y), a in positions:
        if a > largestA:
            largestA = a
            largestPos = x, y
            
    # Nothing found
    if largestA == 0: __reactRot(pmot, tmot, rotation)
    else: __reactPos(pmot, tmot, largestPos)
    
    

#!/usr/bin/python3

''' @file facedetect.py
Defines a class for working with closed loop motor control systems.

@author James McRoberts
@copyright 2020
@date Jun 1st, 2020
@package final'''

# Required imports
import time
import numpy as np
import cv2 as cv
from smbus2 import SMBus, i2c_msg
import struct
import serial

import subprocess
subprocess.run(['sudo', 'chmod', '+666', '/dev/ttyS0'])


TARGET_ADDRESS = 0x02
CAPTURE_COLLECT_OPCODE = 0x01

# Globally exposed variables
camera = cv.VideoCapture(0)
cWidth = camera.get(cv.CAP_PROP_FRAME_WIDTH)
cHeight = camera.get(cv.CAP_PROP_FRAME_HEIGHT)
cOrig = int(cWidth / 2), int(cHeight / 2)
classifier = cv.CascadeClassifier('/usr/local/lib/python3.7/dist-packages/cv2/data/haarcascade_frontalface_alt.xml')
port = serial.Serial("/dev/serial0", timeout=3.0)

def detectFaces(cascade, targetImage, scaleFactor=1.1, neighbors=3):
    ''' Search through an image with the provided cascade classifier, then output the rectangles
    of the faces. '''
    gray = cv.cvtColor(targetImage, cv.COLOR_BGR2GRAY)
    faces = cascade.detectMultiScale(gray, scaleFactor=scaleFactor, minNeighbors=neighbors)

    return faces

def rectCenters(rects):
    ''' Convert rectangles to a tuple of a list of their centers, and a list of their areas. '''
    result = []
    weights= []

    for (x, y, w, h) in rects:
        result.append((x + (w / 2), y + (h / 2)))
        weights.append(w * h)

    return result, weights


def setup():
    ''' Sets up the device and opens devices to the main file in an arduino-ish fashion. '''
    return


def loop():
    ''' Loops to constantly send data to the motor controller MCU. '''
    # Get face coordinates and descriptors
    ret, frame = camera.read()
    rot = cv.getRotationMatrix2D(center=(cWidth / 2, cHeight / 2), angle=90, scale=1.0)
    frame = cv.warpAffine(frame, rot, (int(cHeight), int(cWidth)))
    faces = detectFaces(classifier, cv.flip(frame, flipCode=1))
    centers, weights = rectCenters(faces)
    
    # Convert coordinates
    centered = []
    for (x, y) in centers:
        centered.append((x - cOrig[0], y - cOrig[1]))

    # Send data
    port.write(struct.pack('i', len(centered)))
    idx = 0
    for (x, y) in centered:
        port.write(struct.pack('iii', int(x), int(y), int(weights[idx])))
        idx = idx + 1

    # Display data
    print(centered)
    print(weights)
    print()

if __name__ == '__main__':
    setup()
    while True: loop()

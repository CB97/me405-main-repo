''' @file motor.py
Defines a class for interacting with the IHM04A1 Motor Shield.

@author James Hamilton McRoberts IV
@copyright 2020
@date April 21st, 2020
@package final '''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the IHM04A1 Motor Shield. '''
    in1Pin = None
    in2Pin = None
    enablePin = None
    timerPWM =  None
    pwmChannel1 = None
    pwmChannel2 = None
    
    def __init__(self, enable, in1, in2, timer):
        ''' Creates a motor driver by initializing GPIO pin, and initializes the motor to the 'OFF' state.
        @param enable A pyb.Pin object to use as the enable pin.
        @param in1 A pyb.Pin object to use as the input to half of bridge 1.
        @param in2 A pyb.Pin object to use as the input to half of bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on in1 and in2. '''
        print ('Creating a motor driver.')
        
        # Assign pyb objects
        self.in1Pin = in1
        self.in2Pin = in2
        self.enablePin = enable
        self.timerPWM = timer
        
        # Create pwm channel for later communication
        self.pwmChannel1 = self.timerPWM.channel(1, pyb.Timer.PWM, pin=self.in1Pin)
        self.pwmChannel2 = self.timerPWM.channel(2, pyb.Timer.PWM, pin=self.in2Pin)
        
        # Set the motor low
        self.enablePin.low()
        self.pwmChannel1.pulse_width_percent(0)
        self.pwmChannel2.pulse_width_percent(0)
    
    def enable(self):
        ''' This method enables the movement of the motor. '''
        print('Enabling motor.')
        
        # Set the motor high
        self.enablePin.high()
        
    def disable(self):
        ''' The method disables the movement of the motor. '''
        print('Disabling motor.')
        
        # Set the motor low
        self.enablePin.low()
    
    def setDuty(self, duty):
        ''' This method sets the duty cycle of the motor to the given level. Positive
        values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal to be sent
            to the motor. '''
        
        # Set a timer according to duty's sign
        if duty >= 0:
            self.pwmChannel1.pulse_width_percent(duty)
            self.pwmChannel2.pulse_width_percent(0)
        else:
            self.pwmChannel1.pulse_width_percent(0)
            self.pwmChannel2.pulse_width_percent(-duty)



if __name__ == '__main__' or __name__ == 'builtins':
    # Set up the pins for interfacing
    enable = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    timer = pyb.Timer(3, freq=20000)
    in1 = pyb.Pin(pyb.Pin.cpu.B4)
    in2 = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Init the motor driver
    motor = MotorDriver(enable, in1, in2, timer)
    motor.enable()
    
    # Sweep
    from time import sleep
    for i in range(-4, 5):
        motor.setDuty(i * 25)
        sleep(1)
    
    motor.disable()

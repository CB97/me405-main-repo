var facedetect_8py =
[
    [ "detectFaces", "facedetect_8py.html#a85fbf4a070c0daf30c69f1569cac9252", null ],
    [ "loop", "facedetect_8py.html#a5378c641a3ff73cb3b5211326132ec6a", null ],
    [ "rectCenters", "facedetect_8py.html#a31dff9a775de4daf0b13e3194d46709e", null ],
    [ "setup", "facedetect_8py.html#a16f6fd1cceea2ab5eae1f6f5006d63cc", null ],
    [ "camera", "facedetect_8py.html#a378e64130fa49785fa5caa6b774a48ac", null ],
    [ "CAPTURE_COLLECT_OPCODE", "facedetect_8py.html#af16b770fe2fa46ffa5019eb24a147134", null ],
    [ "cHeight", "facedetect_8py.html#aad5380445c8119cd02a51a3a6892a519", null ],
    [ "classifier", "facedetect_8py.html#ae73c41c4f2ec921e7bdc9457936a6328", null ],
    [ "cOrig", "facedetect_8py.html#a21ca9be014ac51ecf248cb15cd52d606", null ],
    [ "cWidth", "facedetect_8py.html#a815c5c9fb287224c30c8ef0e4edc7d87", null ],
    [ "port", "facedetect_8py.html#ac6686709a610e24441b80940f02f725f", null ],
    [ "TARGET_ADDRESS", "facedetect_8py.html#a8432005b7b7f412e131b71d717665b6c", null ]
];
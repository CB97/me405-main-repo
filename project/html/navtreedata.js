var NAVTREE =
[
  [ "Slug", "index.html", [
    [ "Problem Statement", "index.html#problem", null ],
    [ "Problem Statement - Revisited", "index.html#problem-revisited", null ],
    [ "Fitting In", "index.html#requirements", null ],
    [ "Bill of Materials", "index.html#bom", null ],
    [ "Manufacturing Plan", "index.html#manufacturing", null ],
    [ "Manufacturing Plan - Revisited", "index.html#manufacturing-revisited", null ],
    [ "Safety Assesment", "index.html#safety", null ],
    [ "Safety Assesment - Revisited", "index.html#safety-revisited", null ],
    [ "General Timeline", "index.html#timeline", null ],
    [ "Links", "index.html#links", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
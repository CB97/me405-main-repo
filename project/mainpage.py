''' @file mainpage.py
@author James McRoberts
@mainpage

@section problem Problem Statement
Currently, in terms of autonomous aiming systems, there isn't much in the way
of body mounted autonomous machinery. In order to develop an elegant solution
to a problem like this, fast acting actuators must be used with an auto-calibrating
mount. My proposal, is to build an IMU assisted Pan-Tilt mount, complete with
auto target tracking (powered by a Raspberry Pi, crappy camera, and some basic
ML models for object detection).

@section problem-revisited Problem Statement - Revisited
Everything that was intended to be solved by the problem statement was ALMOST solved.
For some reason, none of my serial ports are working whatsoever, only one motor driver
is working, ad the one that is working occasionally full throttles and doesn't reset
through a soft reset. It has always been a problem in this class with this board,
but it got significantly wose during this. There were times that I couldn't even connect
to the board no matter how hard I tried. Thonny didn't know what to do, the python code
behind it just said IOError, so that's helpful.
The ML that was used endded up being a haar cascade. It's kind of ML, but more weighing
in on the pre-trained side of things. Depth sensing had to be skipped in order to
conserve time and get the printer running. The printer stopped running as previously
alluded to.

@section requirements Fitting In
The way that this project fits into the guidlines, is by using the IMU, encoders
off of the back of the DC motors, and using a new sensor/novel software goal.
I plan on using a simple TF-Lite Model, running on an edge TPU linked through USB
on a Raspberry PI alongside a camera, which seems pretty novel.

@section bom Bill of Materials
+ Raspberry Pi (+)
+ Edge TPU (+)
+ 1kg PLA (+)
+ IMU (+)
+ DC Motors (+)
+ Laser pointers (optional, for better depth sensing) (X - Not Used)
+ Logitech C270 Webcams (++)

@section manufacturing Manufacturing Plan
I have a 3D printer sitting 3 feet to the left of me right now. I also have quite
a few spools of filament. I'm proficient in Fusion360, so I plan on whipping up
a simple mount in that software, exporting it, then printing it; this should
cover packaging. As far as any potential soldering goes, I have a butane soldering
torch that I plan to use for any connections that need strengthening.

@section manufacturing-revisited Manufacturing Plan - Revisited
The manufacturing did remain through my 3D printer. The problem was, any bit of
intricate print was starting to fail. It turns out that my bed is a little bit,
very, warpy. To fix this I ended up attaching a BLTouch to the side of the printer
to make a mesh of the bed. This fixes all of the bed adhesion properties that I was
having issues with before, but it did eat some of my time. The way I had to go about
it to not lost any key safety components was not standard.

@section safety Safety Assesment
This is questionably safe. There are pinch points (not too bad), rotating components
(which aren't big enough to be much of a problem), and the fact that it is going
to aim at people (me) is not the most comforting thing. The most potentially
dangerous part of this project is actually in the manufacturing. The 3D printer
could always just light on fire and I have burnt holes in myself and other things
with the soldering iron before; I still should be fine.

@section safety-revisited Safety Assesment - Revisited
It's not as safe as I had previously thought. Pinch points are an understatement.
These gears have a thirst: wires, blood, skinm insulation, solder. They move fast;
very fast. The motors also get very hot, there are no bearings (only sliprings).

@section timeline General Timeline
+ May 27th, 2020 - Have all electronic components connected/assembled.
+ May 30th, 2020 - Get ML model detecting people and giving cartesian coordinates
    away from the logical center of the image.
+ (X) May 31st, 2020 - Tune PID parameters in control loop. (Full system needed to be build, can't)
+ (V) June 4th, 2020 - Have packaging assebmled.
+ (V) June 6th, 2020 - Testing done.
+ (X) June 12th, 2020 - Implement distance assist with two lazers and basic camera
    geometery.

@section links Links
+ Available Demos: https://archive.org/details/slugggy
+ Source, Notebooks, Models: https://bitbucket.org/CB97/me405-main-repo/src/master/project/ 
'''

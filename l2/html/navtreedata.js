var NAVTREE =
[
  [ "Lab 02 - Incremental Encoders", "index.html", [
    [ "Introduction", "index.html#intro", null ],
    [ "Purpose", "index.html#purpose", null ],
    [ "Usage", "index.html#usage", null ],
    [ "Bugs and Limitations", "index.html#buglimits", null ],
    [ "Location", "index.html#location", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
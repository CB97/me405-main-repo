''' @file encoder.py
Defines a class for interacting with two channel encoders.

@author James McRoberts
@copyright 2020
@date May 14th, 2020
@package encoder'''

import pyb

class MotorEncoder:
    ''' This class implements methods to properly interface with the encoder placed on the back
    of the motors provided in the ME405 lab kit. '''
    # User provided or extrapolated
    pinA = None
    pinB = None
    stdeg = 0.0
    channelA = None
    channelB = None
    encoderTimer = None
    
    # Internal variables needed for function execution
    startPos = 0
    lastDelta = 0
    rawPos = 0
    
    def __init__(self, pinA, pinB, timer, stepdeg):
        ''' Creates a logical motor encoder, setting up the GPIO and timer for use, then binding.
        @param pinA The pin that is responsible for the input signal from channel A of the encoder.
        @param pinB The pin that is responsible for the input signal from channel B of the encoder.
        @param timer The timer number that is responsible for the channels attached.
        @param stepdeg The degrees per step. If set to 0.0, everything works in raw.'''
        print('Creating an encoder driver.')
        
        # Assign PYB objects to self
        self.pinA = pinA
        self.pinB = pinB
        self.encoderTimer = pyb.Timer(timer)
        self.encoderTimer.init(period=0XFFFF, prescaler=1)
        if stepdeg != None: self.stdeg = stepdeg
        
        # Create encoder channels on the timer for capture
        self.channelA = self.encoderTimer.channel(1, pyb.Timer.ENC_AB, pin=self.pinA)
        self.channelB = self.encoderTimer.channel(2, pyb.Timer.ENC_AB, pin=self.pinB)
        
        # Init count
        self.encoderTimer.counter(self.rawPos)
        
    def update(self):
        ''' Updates the position of the attached encoder (as long as it is run relatively frequently).
        @returns The raw value between the timer states (steps).'''
        # Running data
        current = self.encoderTimer.counter()
        left = 0.0
        right = 0.0
        delta = 0.0
        
        # Get shortest distance from old count
        if current < self.rawPos:
            left = self.rawPos - current
            right = (current + 0xFFFF) - self.rawPos
        else:
            left = current - self.rawPos
            right = self.rawPos - (current - 0xFFFF)
        
        if left < right:
            delta = -left
        else:
            delta = right
        
        # Set and return
        self.rawPos = current
        self.lastDelta = delta
        return self.lastDelta
        
        
    def get_position(self):
        ''' Return the most recently updated position of the encoder in degrees.
        @returns The most recently updated position of the encoder in degrees. '''
        result = self.rawPos - self.startPos
        if self.stdeg != 0.0:
            result = result * self.stdeg
        
        return result
        
    def set_position(self, position):
        ''' Sets the logical position of encoder to the degree value provided.
        @param position The value to set the encoder at logically in degrees. '''
        if self.stdeg == 0.0:
            self.startPos = self.rawPos - position
        else:
            self.startPos = self.rawPos - (position / self.stdeg)
        
    def get_delta(self):
        ''' Returns the raw value of steps received from the last update.
        @returns The value of the last update. '''
        return self.lastDelta


if __name__ == '__main__' or __name__ == 'builtins':
    # Running data
    chA = pyb.Pin.cpu.C6
    chB = pyb.Pin.cpu.C7
    timer = 8
    lastVal = None
    
    # Encoder init and use
    encoder = MotorEncoder(chA, chB, timer, None)
    while True:
        encoder.update()
        pos = encoder.get_position()
        if pos == lastVal: continue
        
        lastVal = pos
        print("Pos: " + str(pos))
        from utime import sleep
        sleep(0.5)
    
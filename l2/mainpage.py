''' @file mainpage.py
@author James McRoberts
@mainpage
    
@section intro Introduction
This software driver for handling 2 channel encoders is designed for the
Nucleo-L476RG. It handles this by using an accompanying timer in the MCU
to handle the pulses that come from the encoder.

@section purpose Purpose
The purpose of this driver is to make an easy, portable, way to interact
with the encoders on the back of the motors included in the ME405 lab kit.
It will be used down the line, with the accompanying IMH04A1 motor shield.

@section usage Usage
To create the encoder object, the Encoder() contstructor is used and applies
the number of timer to be initialized, the pins to communicate over, and the
conversion to degrees (optional) to the object. To operate, the Encoder's
update() method must be run somewhat frequently. All accompanying functions
with the driver are pretty self explanatory, they just must use the Update()
function to get any usable data to process.

@section buglimits Bugs and Limitations
After continuous testing, currently there are no known bugs in the driver. As
far as limitations go, the update command is essentially a higher level pull
function. On the lower levels, the timer is counting the encoder pulses to
store a count on interrupt. On the user level, because of the limited 16-bit
counter available to the timer, if the counter were to loop over itself between
updates (not overflow) it would have an inaccurate reading for the delta. This
is an inherent limitation from the design described in the lab, so it should
be up to par.

@section location Location
To get the code for the project, the repository can be found at
https://bitbucket.org/CB97/me405-main-repo/src/master/l2/'''
